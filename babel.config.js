module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        alias: {
          "assets": "./src/assets", // optional
          "components": "./src/components", // optional
          "containers":"./src/containers",
          "screens": "./src/screens", // optional
          "navigators": "./src/navigators", // optional
          "services": "./src/services", // optional
          "images": "./src/assets/images" // optional
        }
      }
    ]
  ]
};
