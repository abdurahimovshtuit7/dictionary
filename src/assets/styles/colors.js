const brandColor = '#7347d3';
const shadowColor = '#7347d3';
const white = '#fff';
const green = '#1B3783'
const grey = '#4c4c4c'
const gray = '#c3c3d7'
const red = '#FF0000'
const placeholder = '#131313'

export default {
  brandColor,
  shadowColor,
  white,
  green,
  grey,
  gray,
  red,
  placeholder
};
