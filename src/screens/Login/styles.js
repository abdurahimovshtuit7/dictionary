import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'space-between',
        backgroundColor:colors.white

    },
    wrapper: {
        // flex:1,
        flexGrow:1,
        paddingVertical: 16
    },
    welcome:{
        marginTop:'33%',
        textAlign:'center',
        fontSize:49,
        marginBottom: 11,
        fontFamily:'WorkSans-Regular',
        fontWeight:'500',
    },
    text:{
        textAlign:'center',
        fontSize:16,
        fontFamily:'WorkSans-Regular',
        color:'rgba(0,0,0,0.6)',
        paddingHorizontal:50,
        marginBottom:'6%'
    },

    forgot:{
        color:colors.brandColor,
        fontSize:12,
        marginHorizontal:'9%',
        textAlign:'right',
        marginBottom:'10%'
    },
    error:{
        color:'#e00a13',
        fontSize: 12,
        marginHorizontal:'9%'
    },
    inputContent: {
        // borderWidth: 1,
        justifyContent:'space-between',
        // borderColor: '#ededed',
        // borderRadius: 25,
        // padding: 4,
        // backgroundColor: '#fff',
        // marginBottom: 12,
        flexDirection: 'row',
    },
    // icon:{
    //     width:'10%',
    //     paddingTop: 49,
    //     marginRight:'9%',
    //     borderWidth:1,
    // },
    icon:{
        width: '10%'
    },


})

export default styles
