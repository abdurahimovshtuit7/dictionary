import React, {Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    StatusBar,
    SafeAreaView
} from 'react-native';
import {SearchBar} from 'react-native-elements';
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";

import styles from './styles';
import Components from 'components'
import NavigationService from 'navigators/NavigationService';
import colors from "../../assets/styles/colors";
import * as Animatable from 'react-native-animatable';
import {Routines} from "services/api";
import {get} from 'lodash'
import Translate from 'services/translateTitle'


import Icon from "react-native-vector-icons/AntDesign"
import Item from "../../components/Item";


class MainPage extends Component {
    state = {
        search: '',
        searchActive: false,
        isFetched: true,
        refreshing: false,
        page: 1,
        array: [1, 2, 3, 4, 5, 6, 7, 9, 10]
    }

    componentDidMount() {
        this.getItems(1)
    }

    getItems(page = 1, isFetched = false, refreshing = true) {

        this.setState({isFetched, refreshing})
        Routines.auth.getItems({
            request: {
                page: page
            }
        }, this.props.dispatch)
            .then((data) => {
                    // let items=get(this.props.items, 'data', [])
                    //     this.renderItems(items)
                    this.setState({isFetched: true, refreshing: false, page: page + 1})
                },
            )
            .catch((e) => {
                this.setState({isFetched: true, refreshing: false})
                console.log('Error:', e)
                let status = get(e, 'message.error', 'Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
            })
    }

    updateSearch = (search) => {
        this.setState({search});
        if (search === '') {
            this.setState({searchActive: !this.state.searchActive})
        }
        //
        // Routines.auth.search({
        //     request: {
        //         search: search
        //     },
        // }, this.props.dispatch)
        //     .then((data) => {
        //             this.setState({searchArray: search});
        //
        //
        //             // let items=get(this.props, 'search', [])
        //             // this.renderItems(items)
        //             // year = this.setState.data.year
        //             // data === "success"?NavigationService.reset('panel', 0): ToastAndroid.show("Siz muvaffaqiyatli ro'yhatdan o'tdingiz !", ToastAndroid.SHORT);
        //
        //         },
        //     )
        //     .catch(e => {
        //         console.log('Error:', e)
        //         let status = get(e, 'message.error', 'Oops... something went wrong!')
        //         if (e.message === 'NETWORK_ERROR') {
        //             Components.Toast.show("Tarmoqqa ulanmagansiz")
        //         } else {
        //             Components.Toast.show(status)
        //         }
        //     })
    };
    data = () => {
        return get(this.props.items, 'data', [])
    }

    render() {
        const {navigation, currentLangCode,items} = this.props
        const {isFetched, refreshing,page} = this.state
        const next = get(items,'pagination.next','')
        // const next = get(items,'pagination.next','')
        console.log("ITEMS:",this.props.items)
        return (
            <View style={styles.parent}>
                <StatusBar backgroundColor={colors.brandColor}
                           barStyle="light-content"
                           translucent={false}/>
                <Animatable.View style={styles.header}>
                    <TouchableOpacity style={styles.icon} onPress={() => {
                        navigation.toggleDrawer()
                    }}>
                        <Icon name={"menu-unfold"} size={29} color={colors.white}/>
                    </TouchableOpacity>
                    {
                        this.state.searchActive ? (
                            <Animatable.View animation="lightSpeedIn" style={styles.animatableView}>
                                <SearchBar
                                    placeholder={'search'}
                                    onChangeText={(value) => this.updateSearch(value)}
                                    value={this.state.search}
                                    lightTheme={true}
                                    inputStyle={{color: '#000',}}
                                    // placeholderStyle={{color:'red'}}
                                    containerStyle={styles.searchContainer}
                                    inputContainerStyle={styles.input}
                                />
                            </Animatable.View>


                        ) : <TouchableOpacity style={styles.icon} onPress={() => {
                            this.setState({searchActive: !this.state.searchActive})
                        }}>
                            <Animatable.View animation="swing">
                                <Icon name={"search1"} size={26} color={colors.white}/>

                            </Animatable.View>
                        </TouchableOpacity>
                    }

                </Animatable.View>

                <Components.InfiniteFlatList
                    contentContainerStyle={styles.container}
                    numColumns={1}
                    // horizontal ={false}
                    // columnWrapperStyle={styles.view}
                    data={this.data()}
                    renderItem={({item, index}) => {
                        return (
                            <Animatable.View
                                key={index}
                                animation="pulse"
                                // duration={index*1000}
                                style={styles.contentItem}
                            >
                                <Components.Item
                                    touch={() => {
                                        NavigationService.navigate('description',{
                                           data:item
                                        })
                                        // (this.state.searchActive)?this.setState({searchActive: !this.state.searchActive}):null

                                    }}
                                    text={Translate.title(currentLangCode, item.russian, item.uzbek, item.english)}
                                />
                            </Animatable.View>
                        )
                    }}
                    loading={!isFetched}
                    refreshing={refreshing}
                    onRefresh={() => {
                        this.getItems(1, true, false)
                    }}
                    onEndReached={() => {
                        // if (next) {
                        //     console.log("PAGE:", page)
                        //     this.getItems(page, false, true)
                        // }
                    }}
                    ListHeaderComponent={({item, index}) => {
                        return null
                    }}
                    emptyText="No Items"
                    keyExtractor={(item, index) =>
                        index
                    }
                    // onEndThreshold={0}
                />

            </View>
        )
            ;
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        items: state.profile.items,
        // search: state.profile.search,
        // filter: state.profile.filter,
        currentLangCode: state.language.lang,
    };
};
MainPage = connect(mapStateToProps)(MainPage)
export default withTranslation('main')(MainPage)
