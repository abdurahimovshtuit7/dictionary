import {StyleSheet,Dimensions} from 'react-native';
import colors from 'assets/styles/colors';
import {horizontal,statusBarHeigth} from "assets/styles/commons";


const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    parent:{
        // paddingTop:statusBarHeigth,
        flex:1,
        backgroundColor:colors.white,

    },
    header:{
        paddingTop:statusBarHeigth*0.75,
        flexDirection:'row',
        backgroundColor:colors.brandColor,
        justifyContent:'space-between',
        alignItems:'center',
        paddingVertical:10,
        paddingBottom:15,
        // borderBottomEndRadius:35,
        // borderBottomLeftRadius:35,
        // height:60
        // paddingHorizontal: 15,
        // paddingVertical: 10,
    },
    container: {
        // flex: 1,
        paddingVertical:10,
        // borderTopRightRadius:20,
        // borderWidth:1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor:colors.white,
    },
    icon:{
        // borderWidth:1,
        paddingVertical:12,
        paddingLeft:15,
        paddingRight:15
    },
    animatableView:{
        width: '82%'
    },
    searchContainer:{
        // marginTop:50,
        // marginLeft:37,
        // marginRight:20,
        marginRight:15,
        marginLeft: 0,
        // marginVertical: 10,
        padding:0,

        backgroundColor:colors.brandColor,
        borderWidth:1,
        borderRadius:60,
        // elevation: 0,
        borderColor:colors.white
    },
    input:{
        backgroundColor:'#fff',
        // borderWidth: 1,
        padding:0,
        borderRadius:50,
        paddingVertical:0,
        height:42,

        // marginVertical:5
    },
    content:{
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:Height*0.12,
        // borderWidth:1,
        // backgroundColor:colors.white,
        // borderRadius: '100%'
    },
    contentItem:{
        // marginVertical:7.5,
        // // borderWidth:1,
        // paddingHorizontal:15,
        // backgroundColor:colors.brandColor,
        // elevation:7,
        // borderRadius:10,
        // paddingVertical:15,
        // paddingHorizontal:15,
    },
    view:{
        justifyContent: 'center',
        flex: 1,
        // borderWidth:1,
        alignItems: 'center',
    },
    touchable:{
        // borderWidth:1,
        paddingVertical:20,
        paddingHorizontal:20,
        width:'80%',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical:15,
        borderRadius:90,
        backgroundColor: colors.brandColor,
        // borderColor:colors.white,
        elevation:9,
    },
    text:{
        color:colors.white,
        fontSize:18,
        fontWeight:'bold'
    }

});
export default styles
