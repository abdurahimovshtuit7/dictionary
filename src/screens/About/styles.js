import {Dimensions, StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    scroll: {
        paddingVertical: 15,
        backgroundColor: colors.white,
        flex:1,
    },
    image:{
        // flex:1,
        paddingVertical:25,
        // justifyContent:'center',
        alignItems:'center',
        elevation: 10,
        shadowColor: colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        marginBottom: Height*0.02
        // borderWidth: 1
    },
    avatar:{
        // ...center,
        width: 120,
        height: 120,
        borderRadius: 10,
    },

    view:{
        // flex:1,
        borderWidth:1,
        shadowColor: colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation:10,
        marginHorizontal: 15,
        borderRadius: 15,
        borderColor: colors.white,
        backgroundColor:colors.white,
        paddingVertical:20,
        marginBottom:Height*0.05,
        paddingHorizontal:15
        // marginVertical:8
    },
})

export default styles
