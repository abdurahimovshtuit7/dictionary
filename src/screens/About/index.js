import React,{Component} from "react";
import {ScrollView, Text, View} from "react-native";
import Components from "../../components";
import styles from "./styles";
// import NavigationService from "../../navigators/NavigationService";
// import * as Animatable from "react-native-animatable";
// import {get} from "lodash";

class About extends Component{
    render() {

        return (
            <ScrollView contentContainerStyle={styles.scroll}>
                <View style={styles.image}>
                    <Components.UserImage username={''} style={styles.avatar}
                                          uri={ require('../../assets/images/Ellipse.png')}
                    />
                </View>
                <View style={styles.view}>
                    <Text>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </Text>

                </View>
            </ScrollView>
        )
    }
}

export default About
