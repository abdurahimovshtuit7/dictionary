import {StyleSheet, Platform} from 'react-native';
import colors from 'assets/styles/colors'
import {Dimensions} from 'react-native';
const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: colors.brandColor
    },
    brandName: {
        fontSize: 49,
        color: colors.white,
        marginTop: 0.2*Height,
        fontFamily: 'WorkSans-Bold'
    },
    activeIndicator: {
      // marginTop:
        marginTop: 0.2*Height,
    },
    footer: {
        fontSize: 12,
        color: colors.white,
        position: 'absolute',
        bottom: 31,
        fontFamily: 'WorkSans-Regular'
    },
    loading: {
        fontSize: 14,
        color: colors.white,
        marginTop: 9,
        fontFamily: 'WorkSans-Regular'
    }

})
export default styles
