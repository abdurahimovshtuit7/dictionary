import {Dimensions, StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:colors.white,
    },
    scroll:{
        marginLeft:0,
        // borderWidth:1,
    },
    wrapper: {
        // flex:1,
        flexGrow:1,
        paddingVertical: 16
    },
    component:{
        paddingHorizontal:25,
        paddingVertical:20,
        // marginBottom:10,
    },
    view:{
        // flex:1,
        borderWidth:1,
        shadowColor: colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation:10,
        marginHorizontal: 15,
        borderRadius: 15,
        borderColor: colors.white,
        backgroundColor:colors.white,
        paddingVertical:20,
        marginBottom:Height*0.05
        // marginVertical:8
    },
    border:{
        borderWidth:0.5,
        flex:1,
        borderColor:colors.border,

    },
    text:{
        fontSize: 12,
        color:colors.textGray,
        fontWeight:'600',
        paddingTop:6,
        textTransform: 'uppercase'
    },
    savedText:{
        fontSize:30,
        fontFamily:'WorkSans-Bold',
        // fontWeight: 'bold'
    },
    image:{
        flex:1,
    },
    row:{
        flexDirection: 'row',
        elevation:10,
        shadowColor: colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        marginHorizontal: 15,
        borderRadius: 15,
        paddingHorizontal:10,
        backgroundColor:colors.white,
        marginBottom: Height*0.03,
        paddingVertical:20,
    },
    icon:{
        position: "absolute",
        bottom: 0,
        right: 0,
        borderWidth:3,
        backgroundColor:colors.brandColor,
        width:35,
        height:35,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:20,
        borderColor:'white',
        // paddingHorizontal: 10,
        // paddingVertical: 10
    },
    username:{
        justifyContent:'center',
        // alignItems: 'center',
        paddingHorizontal: 15,
        width: '78%',
    },
    profile:{
        fontSize:18,
        // fontWeight:'700',
        paddingBottom:5,
        fontFamily:'WorkSans-Bold',
        color:colors.primary,
        // width: '57%',
        // paddingRight:10,
        // borderWidth:1,
    },
    date:{
        fontSize:14,
        color:colors.second,
        fontFamily:'WorkSans-Regular',


    },
    footer:{
        textAlign:'center',
        color:colors.second,
         marginTop:34,
         fontSize:14,
    },
    version:{
        textAlign: 'center',
        fontSize:12,
        color:colors.second,
        paddingTop: 5
    },
    button:{
        marginHorizontal:15,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:colors.background,
        borderRadius:10,
        marginVertical: 35
    },
    logout:{
        paddingVertical: 20,
        fontSize:14,
        color:colors.second
    }

})

export default styles
