import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import Components from '../../components'
import Edit from '../../assets/SVG/Edit'
import {Formik, Field, Form, withFormik} from 'formik';
import * as Animatable from 'react-native-animatable';
import {saveUser} from 'services/actions';


// import moment from "moment";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

//----------------SVG---------------------

import NavigationService from "../../navigators/NavigationService";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";

class Profile extends Component {
    state = {

    }

    render() {
        const {navigation,t,handleSubmit,errors} = this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <View style={styles.component}>
                    <Text style={styles.savedText}>{t('Profile')}</Text>
                </View>

                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled"
                                         enableOnAndroid={false}
                                         contentContainerStyle={styles.wrapper}
                >
                    <Components.Layout>
                        <Animatable.View style={styles.row} animation="pulse">
                            <Components.UserImage style={styles.image}
                                                  uri={ require('../../assets/images/Ellipse.png')}
                                                  username={"Alex Murphy"}
                            >
                                <View style={styles.icon}>
                                    <Edit/>
                                </View>
                            </Components.UserImage>
                            <View style={styles.username}>
                                <Text style={styles.profile} numberOfLines={1}>
                                    Shahboz Abdurahimov
                                </Text>
                                <Text style={styles.date}  numberOfLines={1}>
                                    Registered: June 2, 2020
                                </Text>
                            </View>
                        </Animatable.View>
                        <Animatable.View style={styles.view} animation="pulse">
                            <Field
                                name={'first_name'}
                                label={'first_name'}
                                placeHolder={'Shahboz'}
                                component={Components.EditInput}
                            />
                            <Field
                                name={'last_name'}
                                label={'last_name'}
                                placeHolder={'Abdurahimov'}
                                component={Components.EditInput}
                                hasErrorMessage={false}
                            />
                            <Field
                                name={'phone'}
                                label={'phone number'}
                                // prefix={"+998"}
                                placeHolder={'(+998) 99 999 99 99'}
                                component={Components.EditInput}
                                keyboards={'number-pad'}
                                mask={"(+998) [00] [000] [00] [00]"}
                                iconName={'phone'}
                            />
                            <Field
                                name={'password'}
                                styles={styles.field}
                                label={'password'}
                                // title={t('Parol')}
                                placeHolder={'*****'}
                                component={Components.EditInput}
                                // secureTextEntry={this.state.password}
                                // changeIcon={()=>this._changeIcon()}
                                iconName={this.state.icon}
                            />
                        </Animatable.View>
                        <Components.Submit text={t('saqlash')}
                                           color={colors.white}
                                           padding={'10'}
                                           background = {colors.brandColor}
                                           touch={() => {
                                               NavigationService.navigate('signUp')
                                           }}/>
                        <Components.Submit text={t('log_out')}
                                           color={colors.red}
                                           background = {colors.white}

                                           touch={() => {
                                               this.props.dispatch(saveUser({}))
                                           }}/>

                    </Components.Layout>
                </KeyboardAwareScrollView>
            </SafeAreaView>
        )
    }
}
Profile = withFormik({

    mapPropsToValues: () => ({
        first_name: "Shaxboz",
        last_name:"Abdurahimov",
        // phone:"",
        // password:""
    }),
    validationSchema: ({ t }) => {
        return Yup.object().shape({
            first_name: Yup.string().required("required"),
            // .matches(/^[a-z]+^[A-Z]+$/ , 'Is not in correct format'),
            last_name:Yup.string().required("required"),
            // .matches(/^[a-z]+$/ , 'Is not in correct format'),
            // phone:Yup.string().required("required")
            //     .min(18, 'Belgilar soni kam!'),
            // password:Yup.string()
            //     .min(6, 'Belgilar soni kam!')
            //     .max(16, 'Too Long!')
            //     .required('required'),
        });
    },
    handleSubmit: (values, { props, setSubmitting})=>{
        console.log(values);
        // console.log("err ",props.errors)
        // let {first_name,last_name, phone,password} = values;
        //
        // let onlyNums = phone.replace(/[^\d]/g, '');
        // console.log("PHONE: ", onlyNums)
        // setSubmitting(true);
        // Routines.auth.signUp({request:{
        //         first_name:first_name,
        //         last_name:last_name,
        //         phone:'+'+onlyNums,
        //         password:password
        //     }}, props.dispatch)
        //     .then(() => {
        //         // setSubmitting(false);
        //         // NavigationService.navigate('codeEntry', {
        //         //     phone,
        //         //     countryCode: code
        //         // })
        //     })
        //     .catch(e => {
        //         setSubmitting(false);
        //         if(e.message === "NETWORK_ERROR"){
        //         }
        //     });
    }
})(Profile);
const mapStateToProps = (state, ownProps) => {
    return {

    };
};

Profile = connect(mapStateToProps)(Profile)
export default withTranslation('main')(Profile)
