import React, {Component} from 'react'
import {
    SafeAreaView,
    Text,
    View,
    TouchableOpacity,
    StatusBar,
} from "react-native";
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";
import {Formik, Field, Form, withFormik} from 'formik';
import Icon from 'react-native-vector-icons/Feather';
// import CheckBox from '@react-native-community/checkbox';


import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
import NavigationService from "navigators/NavigationService";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";


class SignUp extends Component {
    state = {
        item1:[
            {label: 'Узбекистан', value: 'uk'},
            {label: 'Казахстан', value: 'france'},
        ],
        item2:[
            {label: 'Ташкент', value: 'uk'},
            {label: 'Джизак', value: 'france'},
        ],
        item3:[
            {label: 'Юнусабадский', value: 'uk'},
            {label: 'Казахстан', value: 'france'},
        ],

    }

    render() {
        const {item1,item2,item3}=this.state
        const {navigation,handleSubmit,errors,route,t} = this.props
        // console.log("params:",route.params.auth)
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false}
                                         contentContainerStyle={styles.wrapper}>
                    <Components.Layout>
                        <Text style={styles.welcome}>
                            Sign Up
                        </Text>
                        <Text style={styles.text}>
                            {t('registration')}
                        </Text>

                        <Field
                            name={'first_name'}
                            label={'first_name'}
                            title={t('first_name')}
                            placeHolder={t('write_here')}
                            component={Components.InputText}
                        />
                        {errors.first_name ? (
                            <Text style={styles.error}>{errors.first_name}</Text>
                        ) : null}
                        <Field
                            name={'last_name'}
                            label={'last_name'}
                            title={t('last_name')}
                            placeHolder={t('write_here')}
                            component={Components.InputText}
                        />
                        {errors.last_name ? (
                            <Text style={styles.error}>{errors.last_name}</Text>
                        ) : null}

                        <Field
                            name={'phone'}
                            label={'phone'}
                            title={t('telefon_raqam')}
                            // placeHolder={t('write_here')}
                            placeHolder={'(+998) 99 999 99 99'}
                            keyboards={'number-pad'}
                            mask={"(+998) [00] [000] [00] [00]"}
                            component={Components.InputText}
                        />
                        {errors.phone ? (
                            <Text style={styles.error}>{errors.phone}</Text>
                        ) : null}

                        <Field
                            name={'name'}
                            label={'name'}
                            title={t('Username')}
                            placeHolder={t('write_here')}
                            component={Components.InputText}
                        />
                        {errors.name ? (
                            <Text style={styles.error}>{errors.name}</Text>
                        ) : null}
                        <Field
                            name={'password'}
                            label={'password'}
                            title={t('Parol')}
                            placeHolder={t('enter_password')}
                            component={Components.InputText}
                        />
                        {errors.password ? (
                            <Text style={styles.error}>{errors.password}</Text>
                        ) : null}
                        <Field
                            name={'confirmPassword'}
                            label={'confirmPassword'}
                            title={t('confirm_password')}
                            placeHolder={t('enter_password')}
                            component={Components.InputText}
                        />
                        {errors.confirmPassword ? (
                            <Text style={styles.error}>{errors.confirmPassword}</Text>
                        ) : null}

                        <Components.Submit text={t('reg')}
                                           color={colors.white}
                                           background={colors.brandColor}
                                           style={styles.component}
                                           isActive={this.props.isSubmitting}
                                           touch={() => {
                                               handleSubmit()
                                               // NavigationService.navigate('selectAuth')
                                           }}/>
                    </Components.Layout>

                </KeyboardAwareScrollView>
            </SafeAreaView>
        )
    }
}


SignUp = withFormik({
    mapPropsToValues: () => ({
        name: "",
        phone:"",
        password: "",
        confirmPassword:"",
        last_name:"",
        first_name:""
    }),
    validationSchema: ({t}) => {
        return Yup.object().shape({
            name: Yup.string().required(t("required")),
            last_name: Yup.string().required(t("required")),
            first_name: Yup.string().required(t("required")),
            // .matches(/^[a-z]+^[A-Z]+$/ , 'Is not in correct format'),
            password: Yup.string()
                .required("Parolni kiriting!")
                .min(8,'8 belgidan kam kiritmang !'),
            confirmPassword: Yup.string()
                .oneOf([Yup.ref('password'), null], 'Passwords must match')
                .required("Parolni kiriting!")
                .min(8,'8 belgidan kam kiritmang !'),
            phone:Yup.string()
                .required("Parolni kiriting!")
        });
    },
    handleSubmit: (values, {props, setSubmitting}) => {
        console.log(values);

        let {name, phone,first_name,last_name,password,confirmPassword} = values;
        // let {route} = this.props
        let onlyNums = phone.replace(/[^\d]/g, '');
        // console.log("Props: ", this.props)
        setSubmitting(true);
        Routines.auth.signUp({
            request: {
                data: {
                    username: name,
                    phone:`+${onlyNums}`,
                    first_name:first_name,
                    last_name:last_name,
                    password1: password,
                    password2:confirmPassword
                }
            }
        }, props.dispatch)
            .then((data) => {
                console.log("Success")
                setSubmitting(false);
                Components.Toast.show("Siz muvaffaqiyatli ro'yxatdan o'tdingiz")
                setTimeout(()=>{
                    NavigationService.goBack()
                },1000)

                // if(get(data.response,'data.success',false)){
                //     NavigationService.navigate('home')
                //     return (
                //         // Components.Toast.show("Siz muvaffaqiyatli ro'yxatdan o'tdingiz")
                //     )
                //
                // } else {
                //     console.log("Success")
                // }
            })
            .catch(e => {
                let status = get(e, 'message.error', 'Oops... something went wrong!')
                if (e.message === 'NETWORK_ERROR') {
                    Components.Toast.show("Tarmoqqa ulanmagansiz")
                } else {
                    Components.Toast.show(status)
                }
                setSubmitting(false);

            });
    }
})(SignUp);
const mapStateToProps = (state, ownProps) => {
    return {};
};

SignUp = connect(mapStateToProps)(SignUp)
export default withTranslation('main')(SignUp)
