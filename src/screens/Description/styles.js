import {Dimensions, StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    scroll: {
        paddingVertical: 15,
        backgroundColor: colors.white,
        flex:1,
    }

})

export default styles
