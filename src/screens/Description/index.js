import React,{Component} from "react";
import {ScrollView, Text, View} from "react-native";
import Components from "../../components";
import styles from "./styles";
import Uzbek from 'assets/SVG/flags/uzbek'
import Uk from 'assets/SVG/flags/uk'
import Russian from 'assets/SVG/flags/russian'
import NavigationService from "../../navigators/NavigationService";
import * as Animatable from "react-native-animatable";
import {get} from "lodash";

class Description extends Component{
    render() {
        const data = this.props.route.params.data
        return (
            <ScrollView contentContainerStyle={styles.scroll}>
                <Components.Description
                    touch={() => {
                        // NavigationService.navigate('description')
                        // (this.state.searchActive)?this.setState({searchActive: !this.state.searchActive}):null
                    }}
                    lang={<Uzbek size={35}/>}
                    text={get(data,'uzbek','')}/>
                <Components.Description
                    touch={() => {
                        // NavigationService.navigate('description')
                        // (this.state.searchActive)?this.setState({searchActive: !this.state.searchActive}):null

                    }}
                    lang={<Uk size={35}/>}
                    text={get(data,'english','')}
                />
                <Components.Description
                    touch={() => {
                        // NavigationService.navigate('description')
                        // (this.state.searchActive)?this.setState({searchActive: !this.state.searchActive}):null

                    }}
                    lang={<Russian size={35}/>}
                    text={get(data,'russian','')}
                />
            </ScrollView>
        )
    }
}

export default Description
