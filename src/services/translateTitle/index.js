const title = (currentLangCode, title_ru, title_uz, title_en, defaultTitle) => {

    switch (true) {
        case (currentLangCode === 'ru' && !!title_ru):
            return title_ru;
        case (currentLangCode === 'uz' && !!title_uz):
            return title_uz;
        case (currentLangCode === 'en' && !!title_en):
            return title_en;
        default: {
            return defaultTitle
        }
    }
};

export default {
    title
}
