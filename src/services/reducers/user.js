import { SAVE_USER,IMAGE_UPLOAD,IMAGE_UP,USER_ID } from '../constants'
//import changeLanguage from '../../components/Language';

const initial = {
    user:{},
    image:{},
    tex_pass:{},
    user_id:{},
};

export default (state = initial, action) => {
    switch (action.type) {
        case SAVE_USER:{
         //   changeLanguage.setLanguage(action.lang)
            return {...state, user: action.user};
        }
        case IMAGE_UPLOAD: {
            return {...state, image: action.image};
        }
        case IMAGE_UP: {
            return {...state, tex_pass: action.tex_pass};
        }
        case USER_ID: {
            return {...state, user_id: action.user_id};
        }
        default:
            return state;
    }
}
