const config = {
    API_ROOT: 'http://2fe3ac1cc8ec.ngrok.io',
    APP_URL: 'http://blabla.uz',
    API_LANGUAGES: [
        {
            id: 1,
            title: 'UZB',
            code: 'uz'
        },
        {
            id: 2,
            title: 'РУС',
            code: 'ru'
        }
    ],
};

export default config;
