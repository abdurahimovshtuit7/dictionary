import { call, put, takeEvery, all } from 'redux-saga/effects'

import { Delete } from '../routines'
import TokenStorage from '../../../TokenStorage';
import {get} from "lodash";

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(Delete.request());
        const currentToken = yield call(TokenStorage.get)

        if(currentToken)
            yield call(api.setToken,currentToken)
        const response = yield call(api.auth.Delete, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                Delete.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(Delete.failure(e))
    } finally {
        yield put(Delete.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(Delete.TRIGGER, trigger, api)
}
