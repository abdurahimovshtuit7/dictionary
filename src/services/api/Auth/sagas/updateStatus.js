import { call, put, takeEvery, all } from 'redux-saga/effects'

import { updateStatus } from '../routines'
import TokenStorage from '../../../TokenStorage';
import {get} from "lodash";

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(updateStatus.request());
        const currentToken = yield call(TokenStorage.get)

        if(currentToken)
            yield call(api.setToken,currentToken)


        const response = yield call(api.auth.updateStatus, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                updateStatus.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(updateStatus.failure(e))
    } finally {
        yield put(updateStatus.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(updateStatus.TRIGGER, trigger, api)
}
