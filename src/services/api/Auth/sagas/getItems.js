import { call, all, put, takeEvery } from 'redux-saga/effects'

import { getItems } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.log(JSON.stringify(request));
    try {
        yield put(getItems.request());

        // const currentToken = yield call(TokenStorage.get)
        //
        // if(currentToken)
        //     yield call(api.setToken,currentToken)

        const response = yield call(api.auth.getItems, request);
        // console.log("wwwww",JSON.stringify(response.data));
        yield all([
            put(
                getItems.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        console.log(e);
        yield put(getItems.failure(e))
    } finally {
        yield put(getItems.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(getItems.TRIGGER, trigger, api)
}
