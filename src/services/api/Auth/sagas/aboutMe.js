import { call, all, put, takeEvery } from 'redux-saga/effects'

import { aboutMe } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.log(JSON.stringify(request));
    try {
        yield put(aboutMe.request());

        const currentToken = yield call(TokenStorage.get)

        if(currentToken)
            yield call(api.setToken,currentToken)

        const response = yield call(api.auth.aboutMe, request);
        // console.log("wwwww",JSON.stringify(response.data));
        yield all([
            put(
                aboutMe.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        console.log(e);
        yield put(aboutMe.failure(e))
    } finally {
        yield put(aboutMe.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(aboutMe.TRIGGER, trigger, api)
}
