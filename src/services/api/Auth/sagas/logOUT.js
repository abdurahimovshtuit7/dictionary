import { call, put, takeEvery, all } from 'redux-saga/effects'

import { logOUT } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(logOUT.request());
        // const currentToken = yield call(TokenStorage.get)

        // if(request.headers.phone){
        //     yield call(api.setPhone,request.headers.phone)
        // }
        // if(request.headers.code)
        //     yield call(api.setKod,request.headers.code)
        //
        // yield call(api.removeToken)
        const currentToken = yield call(TokenStorage.get)

        if(currentToken)
            yield call(api.setToken,currentToken)

        const response = yield call(api.auth.logOUT, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                logOUT.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(logOUT.failure(e))
    } finally {
        yield put(logOUT.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(logOUT.TRIGGER, trigger, api)
}
