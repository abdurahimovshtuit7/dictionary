import { createRoutine, promisifyRoutine } from 'redux-saga-routines'

export const Login = createRoutine('LOGIN');
export const signUp = createRoutine('SIGN_UP');
export const updateDetails = createRoutine('UPDATE_DETAILS');
export const updateStatus = createRoutine('UPDATE_STATUS')
export const editPassword = createRoutine('EDIT_PASSWORD')
export const aboutMe = createRoutine('ABOUT_ME')
export const getItems = createRoutine('GET_ITEMS')
export const search = createRoutine('SEARCH')
export const Delete = createRoutine('DELETE')
export const getPickerData =createRoutine('GET_PICKER_DATA')
export const filter = createRoutine('FILTER')
export const logOUT = createRoutine('LOG_OUT')
export const updateProfile = createRoutine('UPDATE_PROFILE')

export default {
    Login: promisifyRoutine(Login),
    signUp: promisifyRoutine(signUp),
    updateDetails: promisifyRoutine(updateDetails),
    updateStatus: promisifyRoutine(updateStatus),
    editPassword: promisifyRoutine(editPassword),
    aboutMe: promisifyRoutine(aboutMe),
    getItems:promisifyRoutine(getItems),
    search:promisifyRoutine(search),
    Delete:promisifyRoutine(Delete),
    getPickerData:promisifyRoutine(getPickerData),
    filter:promisifyRoutine(filter),
    logOUT:promisifyRoutine(logOUT),
    updateProfile:promisifyRoutine(updateProfile)
}
