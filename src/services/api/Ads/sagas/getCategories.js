import { call, all, put, takeEvery } from 'redux-saga/effects'

import { getCategories } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.log(JSON.stringify(request));
    try {
        yield put(getCategories.request());

        // const currentToken = yield call(TokenStorage.get)
        //
        // if(currentToken)
        //     yield call(api.setToken,currentToken)

        const response = yield call(api.ads.getCategories, request);
        // console.log("wwwww",JSON.stringify(response.data));
        yield all([
            put(
                getCategories.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        console.log(e);
        yield put(getCategories.failure(e))
    } finally {
        yield put(getCategories.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(getCategories.TRIGGER, trigger, api)
}
