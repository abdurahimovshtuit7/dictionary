import { call, all, put, takeEvery } from 'redux-saga/effects'

import { favoritesList } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.log(JSON.stringify(request));
    try {
        yield put(favoritesList.request());

        const currentToken = yield call(TokenStorage.get)

        if(currentToken)
            yield call(api.setToken,currentToken)

        const response = yield call(api.ads.favoritesList, request);
        // console.log("wwwww",JSON.stringify(response.data));
        yield all([
            put(
                favoritesList.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        console.log(e);
        yield put(favoritesList.failure(e))
    } finally {
        yield put(favoritesList.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(favoritesList.TRIGGER, trigger, api)
}
