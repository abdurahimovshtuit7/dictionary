import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
    TouchableOpacity,
    Text,
    StyleSheet
} from 'react-native'

import {get} from 'lodash'
import {createDrawerNavigator} from '@react-navigation/drawer';
import {connect} from "react-redux";
import Animated from "react-native-reanimated";
// import LinearGradient from "react-native-svg/lib/typescript/elements/LinearGradient";


import NavigationService from "./NavigationService";
import {withTranslation} from "react-i18next";

// import colors from 'assets/styles/colors'
//------Screens------------
// import Settings from 'screens/Home/Settings'
import DrawerContent from './DrawerContent';
import MainPage from "screens/MainPage";
import Description from "screens/Description";
import colors from "../assets/styles/colors";
import Profile from "../screens/Profile";
import styles from "./styles";
import Screen from "../components/Layout";
import About from "../screens/About";

const Drawer = createDrawerNavigator();

const Stack = createStackNavigator();

const Main = ({navigation, style}) => {
    return (
        <Animated.View style={StyleSheet.flatten([styles.stack, style])}>
            <Stack.Navigator>
                <Stack.Screen name='Main' component={MainPage}
                              options={{
                                  headerShown: false,
                              }}/>
                <Stack.Screen name='profile' component={Profile}
                              options={{
                                  headerShown: false,
                              }}/>
                <Stack.Screen name='description' component={Description}
                              options={(route) => ({
                                  // headerShown: false,
                                  title: null,
                                  headerBackTitle: "",
                                  headerStyle: {
                                      backgroundColor: colors.brandColor
                                  },
                                  headerBackTitleStyle: {
                                      color: colors.white
                                  },
                                  headerTintColor: colors.white,
                                  headerTitleStyle: {
                                      // fontWeight: 'bold',
                                  },
                              })}/>
                <Stack.Screen name='about' component={About}
                              options={(route) => ({
                                  // headerShown: false,
                                  title: null,
                                  headerBackTitle: "",
                                  headerStyle: {
                                      backgroundColor: colors.brandColor
                                  },
                                  headerBackTitleStyle: {
                                      color: colors.white
                                  },
                                  headerTintColor: colors.white,
                                  headerTitleStyle: {
                                      // fontWeight: 'bold',
                                  },
                              })}/>


            </Stack.Navigator>
        </Animated.View>
    );
}

let DrawerNav = (props) => {
    const [progress, setProgress] = React.useState(new Animated.Value(0))
    const scale = Animated.interpolate(progress, {
        inputRange: [0, 1],
        outputRange: [1, 0.8],
    });
    const borderRadius = Animated.interpolate(progress, {
        inputRange: [0, 1],
        outputRange: [0, 30],
    });
    const screensStyles = {borderRadius, transform: [{scale}]};

    return (
        // <LinearGradient style={{ flex: 1 }} colors={['#E94057', '#4A00E0']}>
        <Drawer.Navigator
            drawerType="slide"
            // hideStatusBar={true}
            overlayColor='transparent'
            contentContainerStyle={{flex: 1}}
            drawerContentOptions={{
                activeBackgroundColor: 'transparent',
                activeTintColor: 'white',
                inactiveTintColor: 'white',
            }}
            sceneContainerStyle={{backgroundColor: colors.white}}
            drawerStyle={styles.drawerStyles}
            drawerContent={props => {
                setProgress(props.progress)
                return (<DrawerContent {...props} />)
            }}
            // hideStatusBar={true}
        >
            <Drawer.Screen name="main"
                           options={{
                               headerShown: false,
                           }}
            >
                {props => <Main {...props} style={screensStyles}/>}
            </Drawer.Screen>

        </Drawer.Navigator>
        // </LinearGradient>
    )
}


const mapStateToProps = (state, ownProps) => {
    return {
        // tabButton: state.tabButton.data,
        // user:state.user.user,

    };
};

DrawerNav = connect(mapStateToProps)(DrawerNav)
export default withTranslation('main')(DrawerNav);
