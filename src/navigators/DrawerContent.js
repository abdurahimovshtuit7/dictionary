import React, {Component, useState, useEffect} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    AsyncStorage,
    Linking, ImageBackground, StatusBar,
} from 'react-native';
import * as Animatable from 'react-native-animatable';

import {
    useTheme,
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    Text,
    TouchableRipple,
    Switch,
} from 'react-native-paper';

import {
    DrawerContentScrollView,
    DrawerItem,
    DrawerItemList,

} from '@react-navigation/drawer';
import Components from 'components';
import {connect} from 'react-redux';
import {withTranslation} from 'react-i18next';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icons from 'react-native-vector-icons/Entypo';
import styles from './styles';
import NavigationService from './NavigationService';
// import LinearGradient from 'react-native-linear-gradient';
import {get} from 'lodash';
import {saveUser} from 'services/actions';
import colors from '../assets/styles/colors';
import {SET_LANGUAGE} from "services/constants";

const TouchableOpac = Animatable.createAnimatableComponent(TouchableOpacity);


class DrawerContent extends Component {
    onLangClicked = (lang) => {
        // console.log("LANG:",lang)
        let {i18n, dispatch} = this.props;
        i18n.changeLanguage(lang, () => {
            dispatch({type: SET_LANGUAGE, lang})
        })
    }


    render() {
        let {userData, t, currentLangCode} = this.props;
        console.log("USER_DATA:", get(userData, 'token', ''))
        console.log("PROGRESS:",this.props.progress)
        return (
            <View style={{flex: 1, marginTop: -4}}>
                {/*<StatusBar backgroundColor={colors.white}*/}
                {/*           barStyle="dark-content"*/}
                {/*           translucent={false}/>*/}
                <DrawerContentScrollView {...this.props}>
                    <View style={styles.drawerContent}>
                        <View style={styles.image}>
                            <Components.UserImage username={''} style={styles.avatar}
                                // uri={get(userData, 'token', '')?(require('assets/images/Ellipse.png')):null}
                            />
                        </View>
                        {
                            (!get(userData, 'token', '')) ? (<View style={styles.row}>
                                <TouchableOpacity style={styles.button} onPress={() => {
                                    NavigationService.navigate('auth');
                                }}>
                                    <Text style={styles.text1}>
                                        {t('Royxatdan_utish')}
                                    </Text>
                                </TouchableOpacity>

                            </View>) : (
                                <Text style={styles.username}>Abdurahimov Shaxboz</Text>
                            )
                        }


                        {/*</LinearGradient>*/}
                    </View>

                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem
                            {...this.props}
                            icon={({color, size}) => (
                                <Icons
                                    name="user"
                                    color={colors.brandColor}
                                    size={size}
                                />
                            )}
                            label={t("profil")}
                            labelStyle={{
                                // fontWeight:'bold',
                                color: colors.brandColor,
                                fontFamily: "WorkSans-Bold",
                            }}
                            onPress={() => {
                                NavigationService.navigate('profile')
                            }}
                        />
                        <DrawerItem
                            {...this.props}
                            icon={({color, size}) => (
                                <Icons
                                    name="info-with-circle"
                                    color={colors.brandColor}
                                    size={size}
                                />
                            )}
                            label={t("about")}
                            labelStyle={{
                                // fontWeight:'bold',
                                color: colors.brandColor,
                                fontFamily: "WorkSans-Bold",
                            }}
                            onPress={() => {NavigationService.navigate('about')}}
                        />
                    </Drawer.Section>
                    <View>
                        <Text style={styles.text}>{t('Til_tanlash')}</Text>
                        <View style={styles.rows}>
                            <TouchableOpac
                                style={[styles.touchable, {backgroundColor: (currentLangCode === 'uz') ? colors.brandColor : colors.gray}]}
                                onPress={() => {
                                    this.onLangClicked('uz')
                                }} animation="zoomIn" duration={1000}>
                                <Text style={styles.langText}>
                                    UZB
                                </Text>
                            </TouchableOpac>
                            <TouchableOpac
                                style={[styles.touchable, {backgroundColor: (currentLangCode === 'ru') ? colors.brandColor : colors.gray}]}
                                onPress={() => {
                                    this.onLangClicked('ru')
                                }} animation="zoomIn" duration={1500}>
                                <Text style={styles.langText}>
                                    РУС
                                </Text>
                            </TouchableOpac>
                            <TouchableOpac
                                style={[styles.touchable, {backgroundColor: (currentLangCode === 'en') ? colors.brandColor : colors.gray}]}
                                onPress={() => {
                                    this.onLangClicked('en')
                                }} animation="zoomIn" duration={2000}>
                                <Text style={styles.langText}>
                                    ENG
                                </Text>
                            </TouchableOpac>
                        </View>
                    </View>
                </DrawerContentScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        // tabButton: state.tabButton.data,
        currentLangCode: state.language.lang,
        userData: state.user.user
    };
};

DrawerContent = connect(mapStateToProps)(DrawerContent);
export default withTranslation('main')(DrawerContent);
