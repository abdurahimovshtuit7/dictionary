import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {connect} from 'react-redux';

//-----Screens-----
import MainPage from 'screens/MainPage'
import DrawerNav from "./DrawerNavigator";
import Login from "screens/Login";
import SignUp from "../screens/SignUp";
import LaunchScreen from "../screens/LaunchScreen";


const Stack = createStackNavigator();

const Auth = (props) => (
    <Stack.Navigator>
        <Stack.Screen name='login' component={Login}
                      options={{
                          headerShown: false,
                      }}/>
        <Stack.Screen name='signUp' component={SignUp}
                      options={{
                          headerShown: false,
                      }}/>

    </Stack.Navigator>
);

class AppNavigators extends Component {
    render() {
        return (
            <Stack.Navigator>
                <Stack.Screen name='launch' component={LaunchScreen}
                              options={(route)=>({
                                  headerShown: false,
                              })}
                />
                <Stack.Screen name='drawer' component={DrawerNav}
                              options={({route}) => ({
                                  headerShown: false,
                              })}
                />
                <Stack.Screen name='auth' component={Auth}
                              options={({route}) => ({
                                  headerShown: false,
                              })}
                />
            </Stack.Navigator>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {};
};

export default connect(mapStateToProps)(AppNavigators);
