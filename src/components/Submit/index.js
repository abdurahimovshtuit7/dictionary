import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import * as Animatable from 'react-native-animatable';

import colors from "assets/styles/colors";

const Submit = (props) => (
    <Animatable.View style={[styles.container,props.style]} animation='bounceInUp' duration={2000}>
        <TouchableOpacity style={[styles.button,{backgroundColor: props.background,borderWidth:props.border?1:0,borderColor:props.border?props.border:null}]} onPress={props.touch}>
            {
                props.isActive?(<ActivityIndicator color={props.color} />):( <Text style={[styles.text,{color:props.color}]}>{props.text}</Text>)
            }

        </TouchableOpacity>
    </Animatable.View>
);
Submit.propTypes = {
    style: PropTypes.object,
    text:PropTypes.string,
    touch:PropTypes.func,
    background:PropTypes.string,
    color:PropTypes.string,
    border:PropTypes.string,
    padding:PropTypes.string
    // margin:PropTypes.string,

};
Submit.defaultProps = {
    style: {},
    text:'',
    touch:function () {},
    background:'',
    color:'',
    border:'',
    padding:"9%"
    // margin:''
};
export default Submit;
