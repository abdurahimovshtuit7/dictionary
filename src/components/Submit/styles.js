import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{

    },
    button:{
        marginHorizontal:"4%",
        paddingVertical:15,
        justifyContent:'center',
        borderRadius:40,
        marginBottom:"5%",

        // marginTop:'8%',
        elevation:10,
        shadowColor: colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
    },
    text:{
        color:colors.white,
        fontSize:16,
        fontFamily:'WorkSans-Bold',
        // fontWeight:'700',
        alignSelf:'center',

        textTransform: 'uppercase'

    }

})

export default styles
