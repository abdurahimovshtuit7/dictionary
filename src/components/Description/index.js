import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import colors from "assets/styles/colors";
import Uzbek from 'assets/SVG/flags/uzbek'
// import Arrow from 'assets/image/SVG/arrow'
// import LinearGradient from "react-native-linear-gradient";

const Item = (props) => (
    <View style={styles.content}>
        <View style={{justifyContent:'flex-end',alignItems:'flex-end',elevation:10}}>
            {/*<View style={styles.icon}>*/}
                {/*<Text style={styles.text2}>*/}
                {props.lang}
                {/*</Text>*/}
            {/*</View>*/}
        </View>

    <TouchableOpacity activeOpacity={1} style={styles.container} onPress={props.touch}>
        <Text style={styles.text}>
            {props.text}
        </Text>

    </TouchableOpacity>
        </View>
);
Item.propTypes = {
    style: PropTypes.object,
    text:PropTypes.string,
    lang:PropTypes.object,
    touch:PropTypes.func,
};
Item.defaultProps = {
    style: {},
    text:'',
    lang:{},
    touch:function () {},
};
export default Item;
