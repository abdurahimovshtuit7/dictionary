import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        marginVertical:7.5,
        // borderWidth:1,
        // borderColor:colors.brandColor,
        // marginHorizontal:15,
        marginRight:15,
        backgroundColor:colors.white,
        borderRadius:15,
        paddingVertical:15,
        paddingHorizontal:15,
        borderTopRightRadius:2,

        shadowColor: colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation:10,
        // borderBottomLeftRadius:5,
    },
    text:{
        color:colors.grey,
        fontSize:16,
        fontFamily:"Rajdhani-Medium",
    },
    content:{
        backgroundColor:'transparent',
        paddingBottom:10,
        paddingHorizontal:15,
        // borderWidth:1,

    },
    icon:{
        // borderWidth:1,
        width:40,
        height:40,
        justifyContent:'center',
        alignItems:'center',
        borderRadius: 30,
        elevation: 10,
        backgroundColor:colors.brandColor
    },
    text2:{
        color:colors.white
    }

})

export default styles
