import { StyleSheet } from 'react-native';
import {container} from 'assets/styles/commons'

export default StyleSheet.create({
    container: {
        ...container
    }
});
