import Layout from './Layout'
import Item  from './Item'
import InfiniteFlatList from "./FlatListInfinity";
import UserImage from './UserImage'
import Input from "./Input";
import Submit from "./Submit";
import InputText from './InputText'
import EditInput from './EditInput'
import Description from "./Description";
import Toast from "react-native-root-toast";



export default {
    Layout,
    Item,
    InfiniteFlatList,
    UserImage,
    Input,
    Submit,
    InputText,
    EditInput,
    Description,
    Toast
}
