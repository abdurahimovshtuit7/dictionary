import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import colors from "assets/styles/colors";
// import Arrow from 'assets/image/SVG/arrow'
// import LinearGradient from "react-native-linear-gradient";

const Item = (props) => (
    <TouchableOpacity style={styles.container} onPress={props.touch}>
        <Text style={styles.text}>
            {props.text}
        </Text>

    </TouchableOpacity>
);
Item.propTypes = {
    style: PropTypes.object,
    text:PropTypes.string,
    touch:PropTypes.func,
};
Item.defaultProps = {
    style: {},
    text:'',
    touch:function () {},
};
export default Item;
