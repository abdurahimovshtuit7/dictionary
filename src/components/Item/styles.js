import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        marginVertical:7.5,
        // borderWidth:1,
        marginHorizontal:15,
        backgroundColor:colors.white,
        elevation:7,
        shadowColor: colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        borderRadius:10,
        paddingVertical:15,
        paddingHorizontal:15,
    },
    text:{
        color:colors.grey,
        fontSize:16,
        fontFamily:"Rajdhani-Medium",
    }

})

export default styles
