import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'


const styles = StyleSheet.create({
    textInput: {
        flexDirection:'row',
        paddingVertical:13,
        justifyContent:'center',
        fontSize: 14,
        // borderWidth: 1,
        fontFamily: 'WorkSans-Regular',
        paddingHorizontal:16,
        // borderRadius:20,
        // borderColor: colors.brandColor,
        backgroundColor: colors.white,
        // shadowColor: 'rgba(0,0,0,0.15)',
        // shadowOffset: { width: 0, height: 0 },
        // shadowOpacity: 0.3,
        // shadowRadius: 10,
        // elevation: 1
    },

    container:{
        marginHorizontal:24,
        // backgroundColor:'rgba(255,255,255,0.61)',
        paddingVertical: 12,
        // borderWidth:1
    },
    title:{
        fontSize:14,
        fontFamily:'WorkSans-Regular',
        color:colors.black,
        paddingBottom:11,
        // paddingBottom:8
    },
    red:{
       color: colors.red,
        // marginLeft:2,
    },
    text:{
        paddingLeft:10,
        fontSize:16,
        fontWeight:'500'
    },
    border:{
        borderWidth:0.7,
        flex:1,
        borderColor:colors.brandColor,
        // marginBottom:'13%'
    },

})

export default styles
